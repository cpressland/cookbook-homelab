directory '/data/sonarr' do
  owner node[:homelab][:user][:apps][:name]
  group node[:homelab][:user][:apps][:name]
  mode 0755
end

docker_image 'linuxserver/sonarr' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[sonarr]', :immediately
end

docker_container 'sonarr' do
  repo 'linuxserver/sonarr'
  tag 'latest'
  volumes [
    '/data/sonarr:/config',
    '/data/downloads:/downloads',
    '/data/media/tv:/tv',
    '/etc/localtime:/etc/localtime:ro'
  ]
  env [
    'TZ=Europe/London',
    "PUID=#{node[:homelab][:user][:apps][:uid]}",
    "PGID=#{node[:homelab][:user][:apps][:gid]}"
  ]
  port '8989:8989'
  restart_policy 'always'
end
