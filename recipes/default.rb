include_recipe      'chef-client::default'
include_recipe      'apt::default'

motd_dir = '/etc/update-motd.d'
[
  "#{motd_dir}/10-help-text",
  "#{motd_dir}/90-updates-available",
  "#{motd_dir}/91-release-upgrade"
].each do |f|
  file f do
    action :delete
  end
end

node[:homelab][:users].each do |u|
  group u[:name] do
    gid u[:gid]
    action :create
  end

  user u[:name] do
    uid u[:uid]
    gid u[:gid]
    shell '/bin/zsh'
    system false
    action :create
  end

  directory "/home/#{u[:name]}" do
    owner u[:name]
    group u[:name]
    mode 0755
  end

  directory "/home/#{u[:name]}/.ssh" do
    owner u[:name]
    group u[:name]
    mode 0700
  end

  file "/home/#{u[:name]}/.ssh/authorized_keys" do
    content u[:authorized_key]
    owner u[:name]
    group u[:name]
    mode 0600
  end

  template "/home/#{u[:name]}/.zshrc" do
    source 'zshrc.erb'
    owner u[:name]
    group u[:name]
    mode 0644
  end
end

group 'sudo' do
  gid 27
  members node[:homelab][:users].map { |u| u[:name] }
  append true
  action :create
end

group node[:homelab][:user][:apps][:name] do
  gid node[:homelab][:user][:apps][:gid]
  members node[:homelab][:users].map { |u| u[:name] }
  append true
  action :create
end

user node[:homelab][:user][:apps][:name] do
  uid node[:homelab][:user][:apps][:uid]
  gid node[:homelab][:user][:apps][:gid]
  system true
  action :create
end

group node[:homelab][:user][:netdata][:name] do
  gid node[:homelab][:user][:netdata][:gid]
  action :create
end

user node[:homelab][:user][:netdata][:name] do
  uid node[:homelab][:user][:netdata][:uid]
  gid node[:homelab][:user][:netdata][:gid]
  system true
  action :create
end

group node[:homelab][:user][:docker][:name] do
  gid node[:homelab][:user][:docker][:gid]
  members node[:homelab][:users].map { |u| u[:name] } + [node[:homelab][:user][:netdata][:name]]
  append true
  action :create
end

template '/etc/sudoers' do
  source 'sudoers.erb'
  verify 'visudo -c -f %{path}'
end

%w(
  htop
  zsh
  vim
  build-essential
  wget
  curl
  tar
).each do |pkg|
  package pkg do
    action :upgrade
  end
end

node[:homelab][:directories].each do |dir|
  directory dir[:name] do
    owner dir[:user]
    group dir[:group]
    mode dir[:mode]
  end
end

docker_service 'default' do
  action [ :create, :start ]
end

remote_file "/usr/sbin/docker-gc" do
  source "https://raw.githubusercontent.com/spotify/docker-gc/master/docker-gc"
  action :create_if_missing
  mode 0755
end

file "/etc/docker-gc-exclude" do
  action :create_if_missing
end

cron_d 'hourly_docker-gc_run' do
  predefined_value "@hourly"
  command "/usr/sbin/docker-gc"
end
