['/data/sabnzbd', '/data/downloads'].each do |d|
  directory d do
    owner node[:homelab][:user][:apps][:name]
    group node[:homelab][:user][:apps][:name]
    mode 0755
  end
end

docker_image 'linuxserver/sabnzbd' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[sabnzbd]', :immediately
end

docker_container 'sabnzbd' do
  repo 'linuxserver/sabnzbd'
  tag 'latest'
  volumes [
    '/data/sabnzbd:/config',
    '/data/downloads:/downloads'
  ]
  env [
    'TZ=Europe/London',
    "PUID=#{node[:homelab][:user][:apps][:uid]}",
    "PGID=#{node[:homelab][:user][:apps][:gid]}"
  ]
  port '8081:8080'
  restart_policy 'always'
end
