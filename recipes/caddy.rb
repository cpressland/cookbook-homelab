['/data/caddy', '/data/caddy/certs', '/data/caddy/logs'].each do |d|
  directory d do
    owner 'root'
    group 'root'
    mode 0755
  end
end

environment = node.chef_environment
template '/data/caddy/Caddyfile' do
  source "caddy/#{environment}.erb"
  owner 'root'
  group 'root'
  mode '0755'
  notifies :redeploy, 'docker_container[caddy]'
end

docker_image 'abiosoft/caddy' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[caddy]', :immediately
end

docker_container 'caddy' do
  repo 'abiosoft/caddy'
  tag 'latest'
  volumes [
    '/data/caddy/Caddyfile:/etc/Caddyfile',
    '/data/caddy/certs:/root/.caddy',
    '/data/caddy/logs:/var/log/caddy'
  ]
  env [
    'ACME_AGREE=true'
  ]
  port [ '80:80', '443:443' ]
  restart_policy 'always'
end
