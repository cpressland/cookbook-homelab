['samba', 'cifs-utils'].each do |p|
  package p do
    action :upgrade
  end
end

service "smbd" do
  action [ :enable, :start ]
end

template '/etc/samba/smb.conf' do
  source 'smb.conf.erb'
  mode 644
  owner 'root'
  group 'root'
  notifies :restart, "service[smbd]", :immediately
end
