['dvb-demod-si2168-b40-01.fw', 'dvb-demod-si2168-02.fw'].each do |f|
  remote_file "/lib/firmware/#{f}" do
    source "https://github.com/OpenELEC/dvb-firmware/raw/master/firmware/#{f}"
    owner 'root'
    group 'root'
    mode 0644
    action :create_if_missing
  end
end

['/data/plex', '/data/plex/config', '/data/plex/transcode'].each do |d|
  directory d do
    owner node[:homelab][:user][:apps][:name]
    group node[:homelab][:user][:apps][:name]
    mode 0755
  end
end

docker_image 'plexinc/pms-docker' do
  tag 'plexpass'
  action :pull
  notifies :redeploy, 'docker_container[plex]', :immediately
end

docker_container 'plex' do
  repo 'plexinc/pms-docker'
  tag 'plexpass'
  volumes [
    '/data/plex/config:/config',
    '/data/plex/transcode:/transcode',
    '/data/media:/data'
  ]
  env [
    'TZ=Europe/London',
    "PLEX_UID=#{node[:homelab][:user][:apps][:uid]}",
    "PLEX_GID=#{node[:homelab][:user][:apps][:gid]}",
    "ALLOWED_NETWORKS=10.0.40.0/24,10.0.41.0/24,10.0.50.0/24"
  ]
  privileged true
  network_mode 'host'
  restart_policy 'always'
end
