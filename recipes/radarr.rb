directory '/data/radarr' do
  owner node[:homelab][:user][:apps][:name]
  group node[:homelab][:user][:apps][:name]
  mode 0755
end

docker_image 'linuxserver/radarr' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[radarr]', :immediately
end

docker_container 'radarr' do
  repo 'linuxserver/radarr'
  tag 'latest'
  volumes [
    '/data/radarr:/config',
    '/data/downloads:/downloads',
    '/data/media/movies:/movies',
    '/etc/localtime:/etc/localtime:ro'
  ]
  env [
    'TZ=Europe/London',
    "PUID=#{node[:homelab][:user][:apps][:uid]}",
    "PGID=#{node[:homelab][:user][:apps][:gid]}"
  ]
  port '7878:7878'
  restart_policy 'always'
end
