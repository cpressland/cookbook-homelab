directory '/data/unifi' do
  owner node[:homelab][:user][:apps][:name]
  group node[:homelab][:user][:apps][:name]
  mode 0755
end

docker_image 'linuxserver/unifi' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[unifi]', :immediately
end

docker_container 'unifi' do
  repo 'linuxserver/unifi'
  tag 'latest'
  volumes [ '/data/unifi:/config' ]
  port [ '8080:8080', '8443:8443' ]
  env [
    "PUID=#{node[:homelab][:user][:apps][:uid]}",
    "PGID=#{node[:homelab][:user][:apps][:gid]}"
  ]
  working_dir '/usr/lib/unifi'
  restart_policy 'always'
end
