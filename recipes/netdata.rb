dependencies = %w{
  zlib1g-dev
  uuid-dev
  libmnl-dev
  gcc
  git
  make
  autoconf
  autogen
  automake
  pkg-config
}

dependencies.each do |pkg|
  package pkg do
    action :upgrade
  end
end

git node[:netdata][:source][:directory] do
  repository node[:netdata][:source][:repo]
  reference  node[:netdata][:source][:branch]
  action     :sync
  notifies   :run, 'execute[netdata_install]', :immediately
end

execute 'netdata_install' do
  cwd     node[:netdata][:source][:directory]
  command "#{node[:netdata][:source][:directory]}/netdata-installer.sh --dont-wait"
  action  :nothing
end
