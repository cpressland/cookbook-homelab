directory '/data/deluge' do
  owner node[:homelab][:user][:apps][:name]
  group node[:homelab][:user][:apps][:name]
  mode 0755
end

docker_image 'linuxserver/deluge' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[deluge]', :immediately
end

docker_container 'deluge' do
  repo 'linuxserver/deluge'
  tag 'latest'
  volumes [
    '/data/deluge:/config',
    '/data/downloads:/data/downloads'
  ]
  env [
    'TZ=Europe/London',
    "PUID=#{node[:homelab][:user][:apps][:uid]}",
    "PGID=#{node[:homelab][:user][:apps][:gid]}",
    'UMASK_SET=022'
  ]
  network_mode 'host'
  restart_policy 'always'
end
