directory '/data/plexpy' do
  owner node[:homelab][:user][:apps][:name]
  group node[:homelab][:user][:apps][:name]
  mode 0755
end

docker_image 'shiggins8/tautulli' do
  tag 'latest'
  action :pull
  notifies :redeploy, 'docker_container[plexpy]', :immediately
end

docker_container 'plexpy' do
  repo 'shiggins8/tautulli'
  tag 'latest'
  volumes [
    '/data/plexpy:/config',
    '/data/plex/config/Library/Application Support/Plex Media Server/Logs:/logs:ro'
  ]
  env [
    'TZ=Europe/London',
    "PUID=#{node[:homelab][:user][:apps][:uid]}",
    "PGID=#{node[:homelab][:user][:apps][:gid]}"
  ]
  port '8181:8181'
  restart_policy 'always'
end
