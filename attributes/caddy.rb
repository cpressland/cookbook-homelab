default[:caddy][:app][:tls_email]           = 'mail@cpressland.io'
default[:caddy][:app][:fqdn]                = 'cpressland.io'
default[:caddy][:app][:logs][:size]         = '100'
default[:caddy][:app][:logs][:keep]         = '10'
