default[:chef_client][:interval] = 7200
default[:chef_client][:splay] = 5

case chef_environment
when 'homelab'
  default[:homelab][:users] = [
    {
      name: 'cpressland',
      uid:  2001,
      gid:  2001,
      authorized_key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCkS0x0QiMDVCprOqoRa7BdKbJK2vmkA0q2hgyFvR2FGk1UT2SCRVfFiACi/ymkbarrrx51e75MQXAPh3KB8BaQvuwVMtp1gh4WvesvkDQ0/i9/MzIjZA/DkMSwY+Zep/umb2A5FfAlq7wqIpAGKQBU73/Etu0AMgUs3GkcJIS9usI6GyLFSqw16OrG7skuZOXaYTv0GXiP/v+HTRsxZcajRmhOa5mu0SKO3x4yEXu3cHAfGjpg+0uH3LA5JcD9TflT5NpB7jy+abfP+HdQfAI9GpS/6fk8QTUo0ffAG7VcKD/fdQLFxamEz3X1LWHdaVFJBByT9bEzlbPgonlOzF47 cp@Chriss-MBP'
    },
    {
      name: 'backwardspy',
      uid:  2002,
      gid:  2002,
      authorized_key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC+Ym30bVVcdzmTeocxGFmgLN7DG1dUaUHM5Dl5CzvdXz/e/AZnvraLDJ+3Qqq1W9DYB0V/KueqjIHzQUzZA2trO9vHGCekUnroKbkXwhDNs7S72tjngtjzoFY8/rdh4uYSTIqgn7mircG7p2v3w/R61xQsJI0RRhS5myjq7Yn80BI1FXJpLGRuCViFUWwt7az4Ei2sW7DOn9B1JJJcHbdig+hqlZ3RMkSiqmhZMUlnmx0y4NI9vrWhzReLRZOPGRlfRSVrYzFuu9tFqW3Q5dUl22ht3BQq3IWfBGZjwXbaA7WugYJ4/6E4wBTvlpQIxYg36K7rP5bF9TCi6RhK3ROr chris@serenity-macbook.lan'
    }
  ]
else
  default[:homelab][:users] = [
    {
      name: 'cpressland',
      uid:  2001,
      gid:  2001,
      authorized_key: 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCkS0x0QiMDVCprOqoRa7BdKbJK2vmkA0q2hgyFvR2FGk1UT2SCRVfFiACi/ymkbarrrx51e75MQXAPh3KB8BaQvuwVMtp1gh4WvesvkDQ0/i9/MzIjZA/DkMSwY+Zep/umb2A5FfAlq7wqIpAGKQBU73/Etu0AMgUs3GkcJIS9usI6GyLFSqw16OrG7skuZOXaYTv0GXiP/v+HTRsxZcajRmhOa5mu0SKO3x4yEXu3cHAfGjpg+0uH3LA5JcD9TflT5NpB7jy+abfP+HdQfAI9GpS/6fk8QTUo0ffAG7VcKD/fdQLFxamEz3X1LWHdaVFJBByT9bEzlbPgonlOzF47 cp@Chriss-MBP'
    }
  ]
end

default[:homelab][:user][:apps][:name] = 'apps'
default[:homelab][:user][:apps][:uid] = 1550
default[:homelab][:user][:apps][:gid] = 1550

default[:homelab][:user][:netdata][:name] = 'netdata'
default[:homelab][:user][:netdata][:uid] = 1552
default[:homelab][:user][:netdata][:gid] = 1552

default[:homelab][:user][:docker][:name] = 'docker'
default[:homelab][:user][:docker][:gid] = 1553

default[:homelab][:directories] = [
  {
    name:      '/data',
    user:      'root',
    group:     'root',
    mode:      '0755'
  },
  {
    name:      '/data/downloads',
    user:      node[:homelab][:user][:apps][:name],
    group:     node[:homelab][:user][:apps][:name],
    mode:      '0755'
  }
]
