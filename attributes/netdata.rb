default[:netdata][:app][:name]         = 'netdata'
default[:netdata][:app][:port]         = '19999'

default[:netdata][:source][:repo]      = 'https://github.com/firehol/netdata.git'
default[:netdata][:source][:branch]    = 'master'
default[:netdata][:source][:directory] = '/usr/local/src/netdata'
