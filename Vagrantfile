# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|

  config.vm.define "default" do |default|
    default.vm.box = "bento/ubuntu-16.04"
    default.vm.hostname = 'default'

    default.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--memory", "1024"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--name", "homelab_default"]
    end

    default.vm.network :private_network, type: 'dhcp'

    default.berkshelf.enabled = true

    default.vm.provision "chef_solo" do |chef|
      chef.roles_path = "roles"
      chef.add_role "default"

      chef.environments_path = "environments"
      chef.environment = "vagrant"

      chef.cookbooks_path = "../"
    end
  end

  config.vm.define "homelab", autostart: false do |homelab|
    homelab.vm.box = "bento/ubuntu-16.04"
    homelab.vm.hostname = 'homelab'

    homelab.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--memory", "4096"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--name", "homelab"]
    end

    homelab.vm.network :private_network, type: 'dhcp'

    homelab.berkshelf.enabled = true

    homelab.vm.provision "chef_solo" do |chef|
      chef.roles_path = "roles"
      chef.add_role "homelab"

      chef.environments_path = "environments"
      chef.environment = "vagrant"

      chef.cookbooks_path = "../"
    end
  end

  config.vm.define "seedbox", autostart: false do |seedbox|
    seedbox.vm.box = "bento/ubuntu-16.04"
    seedbox.vm.hostname = 'seedbox'

    seedbox.vm.provider :virtualbox do |v|
      v.customize ["modifyvm", :id, "--memory", "2048"]
      v.customize ["modifyvm", :id, "--cpus", "2"]
      v.customize ["modifyvm", :id, "--name", "seedbox"]
    end

    seedbox.vm.network :private_network, type: 'dhcp'
    seedbox.vm.network :forwarded_port, guest: 8112, host: 8112

    seedbox.berkshelf.enabled = true

    seedbox.vm.provision "chef_solo" do |chef|
      chef.roles_path = "roles"
      chef.add_role "seedbox"

      chef.environments_path = "environments"
      chef.environment = "vagrant"

      chef.cookbooks_path = "../"
    end
  end

end
